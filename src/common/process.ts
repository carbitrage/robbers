import pairsAndMarkets from '../markets.json';
import pairsNames from '../pairs.json';
import { throwError } from '../common/service';

async function getPairsForMarket(market: Market) {
    const data: MarketPairs[] = pairsAndMarkets as MarketPairs[];
    const pairs = await new Promise(resolve => {
        let entries: string[] = [];
        data.forEach(entry => {
            if (entry.market == market) {
                entries = entry.pairs;
            }
        });
        if (entries.length === 0) {
            throwError(`Market '${market}' not found in 'pairs.json'`);
        }
        else {
            resolve(entries);
        }
    }) as string[];
    return pairs;
}

async function renamePairs(inputData: PairObject[]) {
    const values: PairName[] = pairsNames as PairName[];
    const outputData = await new Promise(resolve => {
        let data = inputData;
        data.forEach(entry => {
            values.forEach(value => {
                if (value.source.includes(entry.pair)) {
                    entry.pair = value.destination;
                }
            });
        resolve(data);
        });
    }) as PairObject[];
    return outputData;
}

export { getPairsForMarket, renamePairs }