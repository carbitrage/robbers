import { delay } from './common/service';
import { createPool } from './common/db';
import { callBinance } from './markets/binance';
import { callBitfinex } from './markets/bitfinex';
import { callPoloniex } from './markets/poloniex';
import { config as dotenvConfig } from 'dotenv';
import { Pool } from 'pg';

async function callMarkets(pool: Pool) {
    callBinance(pool);
    callBitfinex(pool);
    callPoloniex(pool);
}

async function makeCalls() {
    dotenvConfig();
    const pool: Pool = await createPool();
    while(true) {
        await callMarkets(pool);
        await delay(5000);
    }
}

makeCalls();