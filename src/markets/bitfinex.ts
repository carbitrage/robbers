import axios, { AxiosResponse } from 'axios';
import { throwError } from '../common/service';
import { getPairsForMarket, renamePairs } from '../common/process';
import { writeToDb } from '../common/db';
import { Pool } from 'pg';

async function prepareData(raw: BitfinexResponse[]): Promise<PairObject[]> {
    return new Promise(async resolve => {
        const timeNow: string = new Date().toLocaleString();
        const pairs: string[] = await getPairsForMarket(Market.Bitfinex);
        let data: PairObject[] = await new Promise(resolve => {
            let marketData: PairObject[] = [];
            raw.forEach(element => {
                if (pairs.includes(element[0])) {
                    marketData.push({ market: Market.Bitfinex, pair: element[0], price: element[7], date: timeNow });
                }
            });
            if (marketData.length === 0) {
                console.log(`Pairs: ${pairs}\nRaw response data: ${raw}`);
                throwError(`Pairs info not found for GET request to market '${Market.Bitfinex}'`);
            }
            else {
                resolve(marketData);
            }
        }) as PairObject[];
        resolve(data);
    });
}

function callBitfinex(pool: Pool): Promise<void> {
    return new Promise(resolve => {
        axios.get('https://api-pub.bitfinex.com/v2/tickers?symbols=tBTCUST,tETHUST,tLTCUST,tNEOUSD,tXRPUSD')
            .then(async function (response: AxiosResponse) {
                let data = await prepareData(response.data as BitfinexResponse[]);
                data = await renamePairs(data);
                await writeToDb(pool, data);
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Bitfinex is unreachable or something else gone wrong. ${error}`);
                resolve();
            });
    });
}

export { callBitfinex };