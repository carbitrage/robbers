declare interface BinanceResponse {
    symbol: string;
    price: string;
}

declare type BitfinexResponse = [
    string, // SYMBOL (0)
    number, // BID (1)
    number, // BID_SIZE (2)
    number, // ASK (3)
    number, // ASK_SIZE (4)
    number, // DAILY_CHANGE (5)
    number, // DAILY_CHANGE_RELATIVE (6)
    number, // LAST_PRICE (7)
    number, // VOLUME (8)
    number, // HIGH (9)
    number  // LOW (10)
]

declare interface PoloniexPair {
    id: number;
    last: string;
    lowestAsk: string;
    highestBid: string;
    percentChange: string;
    baseVolume: string;
    quoteVolume: string;
    isFrozen: string;
    high24hr: string;
    low24hr: string;
}

declare type PoloniexResponse = {
    [key: string]: PoloniexPair;
}

declare interface MarketPairs {
    market: string;
    pairs: string[];
}

declare interface PairObject {
    market: string;
    pair: string;
    price: number;
    date: string;
}

declare interface PairName {
    source: string[];
    destination: string;
}

declare const enum Market { Binance = 'Binance', Bitfinex = 'Bitfinex', Poloniex = "Poloniex" }